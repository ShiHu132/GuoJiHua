//
//  ViewController.m
//  测试国际化
//
//  Created by mac on 2017/4/10.
//  Copyright © 2017年 mac. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    UILabel *lable  = [[UILabel alloc]init];
    lable.text = NSLocalizedString(@"ContactsBackup", nil);
   
    lable.backgroundColor = [UIColor redColor];
    lable.frame = CGRectMake(10, 100, 200, 100);
    [self.view addSubview:lable];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
